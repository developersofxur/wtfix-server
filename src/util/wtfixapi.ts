import {
  DestinyDamageTypeDefinition,
  DestinyInventoryItemDefinition,
  DestinyItemInstanceComponent,
  DestinyItemReusablePlugsComponent,
  DestinyItemSocketsComponent,
  DestinyItemStatsComponent,
  DestinyStatDefinition,
  DestinyVendorDefinition,
} from "bungie-api-ts/destiny2";
import { XurLocationStatus } from "./xur";

async function getManifestItem(table: string, itemHash: number | string) {
  let resp = await fetch(`http://api/manifest/${table}/${itemHash}`);
  let jsonbody = await resp.json();
  return jsonbody;
}

export async function getInventoryItemDef(
  itemHash: number,
): Promise<DestinyInventoryItemDefinition> {
  return getManifestItem("DestinyInventoryItemDefinition", itemHash);
}

// async getInventoryItemFromVendor(
//   vendorHash: number,
//   itemIndex: number,
// ): Promise<DestinyInventoryItemDefinition> {
//   let itemHash = (
//     await this._getManifestItem("DestinyVendorDefinition", vendorHash)
//   ).itemList[itemIndex].itemHash;
//   return await this._getManifestItem(
//     "DestinyInventoryItemDefinition",
//     itemHash,
//   );
// }

export async function getVendorDef(
  vendorHash: number,
): Promise<DestinyVendorDefinition> {
  return getManifestItem("DestinyVendorDefinition", vendorHash);
}

export async function getDamageTypeDef(
  damageTypeHash: number,
): Promise<DestinyDamageTypeDefinition> {
  return getManifestItem("DestinyDamageTypeDefinition", damageTypeHash);
}

export async function getStatDef(
  statHash: number,
): Promise<DestinyStatDefinition> {
  return getManifestItem("DestinyStatDefinition", statHash);
}

export async function getXurStatus(): Promise<XurLocationStatus> {
  let resp = await fetch("http://api/xur/status");
  let jsonbody = await resp.json();
  return jsonbody;
}

export async function getXurInventory(): Promise<XurInventory> {
  let resp = await fetch("http://api/xur/inventory");
  let jsonbody = await resp.json();
  return jsonbody;
}

export interface XurInventory {
  weapons: ItemGroups;
  armor: ItemGroups;
}

export interface ItemGroups {
  shared: Item[];
  warlock: Item[];
  hunter: Item[];
  titan: Item[];
}

export interface Item {
  itemHash: number;
  instance: DestinyItemInstanceComponent;
  stats: DestinyItemStatsComponent;
  sockets: DestinyItemSocketsComponent;
  reusablePlugs: DestinyItemReusablePlugsComponent;
}
