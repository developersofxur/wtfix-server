export const THEMES = [{
    name: 'match',
    icon: '/images/themes/match.png'
}, {
    name: 'light',
    icon: '/images/themes/light.png'
}, {
    name: 'dark',
    icon: '/images/themes/dark.png'
}, {
    name: 'christmas',
    icon: '/images/themes/christmas.png'
}, {
    name: 'heart',
    icon: '/images/themes/heart.png'
}, {
    name: 'crimson-days',
    icon: '/images/themes/crimson-days.png'
}]