import { revalidatePath } from "next/cache";
import { NextRequest } from "next/server";

export async function GET(req: NextRequest) {
  const key = req.nextUrl.searchParams.get("key");

  if (key === null || key !== process.env["API_KEY"]) {
    console.error("Invalid key");
    console.error(req.nextUrl);
    return new Response("You shouldn't be here", { status: 400 });
  }

  console.log("Refreshing site");

  revalidatePath("/");
  revalidatePath("/bot");
  revalidatePath("/app");
  revalidatePath("/contributors");
  revalidatePath("/friends");
  revalidatePath("/faq");

  return new Response("Refreshed site, thanks");
}
