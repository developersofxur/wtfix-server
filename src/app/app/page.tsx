import { TextBlock } from "@/components/text-block";
import Link from "next/link";
import Image from "next/image"

export default async function App() {
	return (
		<>
		  <h1 className="text-4xl font-bold text-[--special-text-color]">Welcome to <span className="fancy-text-anim">The Future</span></h1>
		  <p className="mt-2">{"We at WTFIX are always looking forward.  We want to be on the cutting edge of what's possible, so we can tell you where Xûr is just that much better.  On June 5th last year, we were shown a Vision of what that cutting edge looks like.  A product more advanced, more immersive, and more expensive than anything we'd seen before.  And we knew we needed to be a part of it."}</p>
			<Image className="mt-2" src="/images/vision-pro-app-screenshot.png" width={960} height={540} alt={"Screenshot of the WTFIX Apple Vision Pro app"} />
			<p className="mt-2">{"Introducing, WTFIX for the Apple Vision Pro.  The world's first Xûr finding app for VR, AR, MR, XR, all the other Rs, and Spatial Computing.  A real app that you can actually download, WTFIX is beautifully hand crafted to feel right at home in visionOS.  Its intuitive controls allow for opening and closing the map with ease.  And as always, the very first thing you see when you open the app is where the fuck Xûr is.  WTFIX is such a deeply immersive, intuitive, fulfilling experience on Apple Vision Pro, that we believe it represents a step forward for the headset itself.  Every Apple Vision Pro owner, as well as Apple themselves, have been searching for the reason to use the headset.  Reasons like productivity, movie watching, in-car entertainment device for drivers of moving vehicles have been thrown around, but nothing has stuck.  We believe we have found the reason.  And the reason is Xûr."}</p>
			<p className="mt-2">If you are <s>rich enough</s> blessed enough to own one of these beautiful devices, the link to the app is right <Link href="https://apps.apple.com/us/app/wtfix/id6477733182" className="text-xl">HERE</Link>.  For the rest of you plebs, a demo video can be found <Link href="https://youtu.be/YItEiYfWIho" className="text-sm">here</Link></p>
		</>
	)
}